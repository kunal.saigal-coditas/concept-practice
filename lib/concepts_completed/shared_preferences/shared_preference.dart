import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPref extends StatefulWidget {
  const SharedPref({super.key});

  @override
  State<SharedPref> createState() => _SharedPrefState();
}

class _SharedPrefState extends State<SharedPref> {
  final _userNameController = TextEditingController();
  final _userAgeController = TextEditingController();
  late SharedPreferences _preferences;

  String? savedUsername;
  int? savedUserAge;
  Future<void> _loadUserData() async {
    _preferences = await SharedPreferences.getInstance();
    savedUsername = _preferences.getString('username');
    savedUserAge = _preferences.getInt('age');

    setState(() {
      if (savedUsername != null) {
        _userNameController.text = savedUsername!;
      }
      if (savedUserAge != null) {
        _userAgeController.text = savedUserAge.toString();
      }
    });
  }

  void _saveUserData() {
    String username = _userNameController.text;
    int? userAge = int.tryParse(_userAgeController.text);

    if (username.isNotEmpty && userAge != null) {
      _preferences.setString('username', username);
      _preferences.setInt('age', userAge);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadUserData();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _userNameController.dispose();
    _userAgeController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text("Shared Preference")),
        body: Center(
            child: Padding(
          padding: const EdgeInsets.all(15),
          child: Column(
            children: [
              TextFormField(
                decoration: InputDecoration(labelText: "Enter Your Name"),
                controller: _userNameController,
              ),
              SizedBox(height: 15),
              TextFormField(
                decoration: InputDecoration(labelText: "Enter Your Age"),
                controller: _userAgeController,
              ),
              SizedBox(height: 20),
              ElevatedButton(onPressed: _saveUserData, child: Text("Submit")),
              SizedBox(height: 20),
              Text("Saved Name is: ${savedUsername}"),
              SizedBox(width: 10),
              Text("Saved Age is: ${savedUserAge}")
            ],
          ),
        )),
      ),
    );
  }
}
