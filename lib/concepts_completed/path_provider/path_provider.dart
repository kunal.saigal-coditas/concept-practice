import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

class PathPro extends StatefulWidget {
  const PathPro({super.key});

  @override
  State<PathPro> createState() => _PathProState();
}

class _PathProState extends State<PathPro> {
  String _documentsPath = '';

  Future<void> _getDocumentsPath() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    setState(() {
      _documentsPath = documentsDirectory.path;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text("Path Provider")),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Documents Directory Path:'),
              Text(
                _documentsPath,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
              ),
              SizedBox(height: 20),
              ElevatedButton(
                onPressed: _getDocumentsPath,
                child: Text('Get Documents Directory'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
