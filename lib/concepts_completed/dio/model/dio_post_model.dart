class DioPostModel {
  int postId;
  int id;
  String name;
  String email;
  String body;

  DioPostModel({
    required this.postId,
    required this.id,
    required this.name,
    required this.email,
    required this.body,
  });

  factory DioPostModel.getData(Map<dynamic, dynamic> json) {
    return DioPostModel(
        postId: json["postId"],
        id: json["id"],
        name: json["name"],
        email: json["email"],
        body: json["body"]);
  }
}
