import 'package:dio/dio.dart';

import '../model/dio_post_model.dart';

class DioRepo {
  List<DioPostModel> postlist = [];

  Future<List<DioPostModel>> getDatafromDio() async {
    Dio dio = Dio();
    dio.interceptors.add(
      InterceptorsWrapper(
        onResponse: (response, h) {
          for (Map l in response.data) {
            postlist.add(DioPostModel.getData(l));
          }
          return h.next(response);
        },
      ),
    );
    try {
      Response response =
          await dio.get("https://jsonplaceholder.typicode.com/comments");

      if (response.statusCode == 200) {
        return postlist;
      } else {
        return [];
      }
    } catch (e) {
      print(e.toString());
      return [];
    }
  }
}
