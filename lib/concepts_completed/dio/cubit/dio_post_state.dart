part of 'dio_post_cubit.dart';

@immutable
abstract class DioPostState {}

class DioPostLoadingState extends DioPostState {}

class DioPostLoadedState extends DioPostState {
  final List<DioPostModel> postList;

  DioPostLoadedState(this.postList);
}

class DioPostErrorState extends DioPostState {
  final String errorMessage;

  DioPostErrorState(this.errorMessage);
}
