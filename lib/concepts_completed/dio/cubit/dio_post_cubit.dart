import 'package:bloc/bloc.dart';
import 'package:concept_practice/concepts_completed/dio/model/dio_post_model.dart';
import 'package:concept_practice/concepts_completed/dio/repo_logic/dio_repo.dart';
import 'package:meta/meta.dart';

part 'dio_post_state.dart';

class DioPostCubit extends Cubit<DioPostState> {
  DioPostCubit() : super(DioPostLoadingState()) {
    fetchpost();
  }
  DioRepo diorepo = DioRepo();
  void fetchpost() async {
    try {
      List<DioPostModel> postList = await diorepo.getDatafromDio();
      emit(DioPostLoadedState(postList));
    } catch (e) {
      print(e.toString());
      emit(DioPostErrorState(e.toString()));
    }
  }
}
