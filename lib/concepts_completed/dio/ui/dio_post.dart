import 'package:concept_practice/concepts_completed/dio/cubit/dio_post_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:http/http.dart';

class DioPostFetch extends StatefulWidget {
  const DioPostFetch({super.key});

  @override
  State<DioPostFetch> createState() => _DioPostFetchState();
}

class _DioPostFetchState extends State<DioPostFetch> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => DioPostCubit(),
      child: MaterialApp(
        home: Scaffold(
          appBar: AppBar(title: const Text("Dio Data Fetching")),
          body: Padding(
            padding: const EdgeInsets.all(8),
            child: BlocBuilder<DioPostCubit, DioPostState>(
              builder: (context, state) {
                if (state is DioPostLoadingState) {
                  return Center(child: CircularProgressIndicator());
                }
                if (state is DioPostLoadedState) {
                  return ListView.builder(
                    itemCount: state.postList.length,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.all(5),
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black),
                              borderRadius: BorderRadius.circular(20)),
                          padding: const EdgeInsets.all(8),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('PostID : ${state.postList[index].postId}'),
                              const SizedBox(height: 10),
                              Text('ID : ${state.postList[index].id}'),
                              const SizedBox(height: 10),
                              Text('Name : ${state.postList[index].name}'),
                              const SizedBox(height: 10),
                              Text('Email : ${state.postList[index].email}'),
                              const SizedBox(height: 10),
                              Text('Body : ${state.postList[index].body}'),
                              const SizedBox(height: 10),
                            ],
                          ),
                        ),
                      );
                    },
                  );
                }
                if (state is DioPostErrorState) {
                  Center(child: Text(state.errorMessage));
                }
                return const Center(child: Text("Error has Occured"));
              },
            ),
          ),
        ),
      ),
    );
  }
}
