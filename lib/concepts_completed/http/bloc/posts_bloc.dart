import 'dart:async';
// import 'dart:convert';

import 'package:bloc/bloc.dart';
// import 'package:concept_practice/http/repo_logic/posts_repo.dart';
import 'package:meta/meta.dart';
// import 'package:http/http.dart' as http;
import '../model/http_post_model.dart';
import '../repo_logic/posts_repo.dart';

part 'posts_event.dart';
part 'posts_state.dart';

class PostsBloc extends Bloc<PostsEvent, PostsState> {
  PostsBloc() : super(PostFetchingLoadingState()) {
    on<InitialFetchingEvent>(initialFetchingEvent);
    on<PostAddEvent>(postAddEvent);
  }

  Future<FutureOr<void>> initialFetchingEvent(
      InitialFetchingEvent event, Emitter<PostsState> emit) async {
    emit(PostFetchingLoadingState());
    List<PostModel> fetchedposts = await PostRepo.fetchPosts();
    emit(PostFetchingSuccessfullState(posts: fetchedposts));
  }

  FutureOr<void> postAddEvent(
      PostAddEvent event, Emitter<PostsState> emit) async {
    bool addingSuccess = await PostRepo.addPost();
    if (addingSuccess == true) {
      print(addingSuccess.toString());
    } else {
      emit(PostErrorState());
    }
  }
}
