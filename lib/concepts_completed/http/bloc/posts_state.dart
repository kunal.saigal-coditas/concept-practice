part of 'posts_bloc.dart';

@immutable
abstract class PostsState {}

class PostErrorState extends PostsState {}

class PostFetchingLoadingState extends PostsState {}

class PostFetchingSuccessfullState extends PostsState {
  final List<PostModel> posts;
  PostFetchingSuccessfullState({required this.posts});
}
