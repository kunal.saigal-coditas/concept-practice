part of 'posts_bloc.dart';

@immutable
abstract class PostsEvent {}

class InitialFetchingEvent extends PostsEvent {}

class PostAddEvent extends PostsEvent {}
