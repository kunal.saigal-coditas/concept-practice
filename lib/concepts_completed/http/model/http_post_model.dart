class PostModel {
  int userId;
  String title;
  String body;

  PostModel({
    required this.userId,
    required this.title,
    required this.body,
  });

  factory PostModel.getData(Map<dynamic, dynamic> json) {
    return PostModel(
        userId: json["userId"], title: json["title"], body: json["body"]);
  }
}
