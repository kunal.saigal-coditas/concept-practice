// import 'dart:convert';
import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:concept_practice/http/bloc/posts_bloc.dart';
// import 'package:concept_practice/http/model/http_post_model.dart';
import 'package:flutter/material.dart';

import '../bloc/posts_bloc.dart';
// import 'package:http/http.dart' as http;

class HTTPGrid extends StatefulWidget {
  const HTTPGrid({super.key});

  @override
  State<HTTPGrid> createState() => _HTTPGridState();
}

class _HTTPGridState extends State<HTTPGrid> {
  final PostsBloc postBloc = PostsBloc();

  // late Future<List<Posts>> fetchposts;
  // @override
  // void initState() {
  //   super.initState();
  //   fetchposts = fetchPosts();
  // }

  @override
  void initState() {
    postBloc.add(InitialFetchingEvent());
    super.initState();
  }

  // List<PostModel> fetchedposts = [];

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => PostsBloc(),
      child: MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            title: const Text("Http Data Fetching"),
          ),
          floatingActionButton: FloatingActionButton(
              onPressed: () {
                postBloc.add(PostAddEvent());
              },
              child: const Icon(Icons.add)),
          body: BlocConsumer<PostsBloc, PostsState>(
            bloc: postBloc,
            listenWhen: (previous, current) =>
                current is PostFetchingLoadingState,
            buildWhen: (previous, current) =>
                current is! PostFetchingLoadingState,
            listener: (context, state) {},
            builder: (context, state) {
              switch (state.runtimeType) {
                case PostFetchingLoadingState:
                  return const Center(child: CircularProgressIndicator());
                case PostFetchingSuccessfullState:
                  final successState = state as PostFetchingSuccessfullState;
                  return ListView.builder(
                      itemCount: successState.posts.length,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.all(5),
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.black),
                                borderRadius: BorderRadius.circular(20)),
                            padding: const EdgeInsets.all(6),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    'UserId : ${successState.posts[index].userId}'),
                                const SizedBox(height: 10),
                                Text(
                                    'Title : ${successState.posts[index].title}'),
                                const SizedBox(height: 10),
                                Text(
                                    'Body : ${successState.posts[index].body}'),
                                const SizedBox(height: 10),
                              ],
                            ),
                          ),
                        );
                      });
                default:
                  return const SizedBox();
              }

              // return FutureBuilder(
              //         future: fetchposts,
              //         builder: (context, snapshot) {
              //           if (snapshot.hasData) {
              //             return ListView.builder(
              //                 itemCount: fetchedposts.length,
              //                 itemBuilder: (context, index) {
              //                   return Padding(
              //                     padding: const EdgeInsets.all(5),
              //                     child: Container(
              //                       decoration: BoxDecoration(
              //                           border: Border.all(color: Colors.black),
              //                           borderRadius: BorderRadius.circular(20)),
              //                       padding: const EdgeInsets.all(6),
              //                       child: Column(
              //                         mainAxisAlignment: MainAxisAlignment.center,
              //                         crossAxisAlignment: CrossAxisAlignment.start,
              //                         children: [
              //                           Text(
              //                               'UserId : ${fetchedposts[index].userId}'),
              //                           const SizedBox(height: 10),
              //                           Text('Title : ${fetchedposts[index].title}'),
              //                           const SizedBox(height: 10),
              //                           Text('Body : ${fetchedposts[index].body}'),
              //                           const SizedBox(height: 10),
              //                         ],
              //                       ),
              //                     ),
              //                   );
              //                 });
              //           } else {
              //             return const Center(child: CircularProgressIndicator());
              //           }
              //         });
            },
          ),
        ),
      ),
    );
  }

  // Future<List<PostModel>> fetchPosts() async {
  //   final response =
  //       await http.get(Uri.parse('https://jsonplaceholder.typicode.com/posts'));
  //   final data = jsonDecode(response.body.toString());

  //   if (response.statusCode == 200) {
  //     for (Map index in data) {
  //       fetchedposts.add(PostModel.getData(index));
  //     }
  //     return fetchedposts;
  //   } else {
  //     throw Exception('Failed to load album');
  //   }
  // }
}
