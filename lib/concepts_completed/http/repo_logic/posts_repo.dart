import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import '../model/http_post_model.dart';

class PostRepo {
  static Future<List<PostModel>> fetchPosts() async {
    List<PostModel> fetchedposts = [];
    try {
      final response = await http
          .get(Uri.parse('https://jsonplaceholder.typicode.com/posts'));
      final data = jsonDecode(response.body.toString());
      for (Map index in data) {
        fetchedposts.add(PostModel.getData(index));
      }
      return fetchedposts;
    } catch (e) {
      print(e.toString());
      return [];
    }
  }

  static Future<bool> addPost() async {
    try {
      var response = await http
          .post(Uri.parse('https://jsonplaceholder.typicode.com/posts'), body: {
        "title": "Kunal",
        "body": "Kunal is learning Flutter",
        "userId": "34",
      });
      if (response.statusCode >= 20 && response.statusCode < 300) {
        return true;
      } else
        return false;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }
}
