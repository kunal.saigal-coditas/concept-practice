import 'package:concept_practice/concepts_completed/dio/ui/dio_post.dart';
import 'package:concept_practice/concepts_completed/http/ui/http_post.dart';
import 'package:concept_practice/concepts_completed/path_provider/path_provider.dart';
import 'package:concept_practice/concepts_completed/shared_preferences/shared_preference.dart';
import 'package:concept_practice/defaultNavPage.dart';
import 'package:concept_practice/error/errorPage.dart';
import 'package:concept_practice/routing/routeNames.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class AppGoRoutes {
  GoRouter router = GoRouter(
    routes: [
      GoRoute(
        path: "/",
        name: "App Navigation",
        pageBuilder: (context, state) {
          return const MaterialPage(child: MyAppNav());
        },
      ),
      GoRoute(
        path: '/httpDataFetching',
        name: RouteNames.httpDataFetching,
        pageBuilder: (context, state) {
          return const MaterialPage(child: HTTPGrid());
        },
      ),
      GoRoute(
        path: '/dioDataFetching',
        name: RouteNames.dioDataFetching,
        pageBuilder: (context, state) {
          return const MaterialPage(child: DioPostFetch());
        },
      ),
      GoRoute(
        path: '/sharedPreferences',
        name: RouteNames.sharedPreferneces,
        pageBuilder: (context, state) {
          return const MaterialPage(child: SharedPref());
        },
      ),
      GoRoute(
        path: '/pathProvider',
        name: RouteNames.pathProvider,
        pageBuilder: (context, state) {
          return const MaterialPage(child: PathPro());
        },
      )
    ],
    errorPageBuilder: (context, state) {
      return const MaterialPage(child: ErrorPage());
    },
  );
}
