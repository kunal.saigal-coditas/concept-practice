class RouteNames {
  static const String httpDataFetching = "httpDataFetching";
  static const String dioDataFetching = "dioDataFetching";
  static const String sharedPreferneces = "sharedPreferences";
  static const String pathProvider = "pathProvider";
}
