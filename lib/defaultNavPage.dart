import 'package:concept_practice/routing/routeNames.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class MyAppNav extends StatelessWidget {
  const MyAppNav({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: const Text("Navigation")),
        body: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ElevatedButton(
                onPressed: () {
                  GoRouter.of(context).pushNamed(RouteNames.httpDataFetching);
                },
                child: const Text("Http Data Fetching")),
            const SizedBox(height: 20),
            ElevatedButton(
                onPressed: () {
                  GoRouter.of(context).pushNamed(RouteNames.dioDataFetching);
                },
                child: const Text("Dio Data Fetching")),
            const SizedBox(height: 20),
            ElevatedButton(
                onPressed: () {
                  GoRouter.of(context).pushNamed(RouteNames.sharedPreferneces);
                },
                child: const Text("Shared Preferences")),
            const SizedBox(height: 20),
            ElevatedButton(
                onPressed: () {
                  GoRouter.of(context).pushNamed(RouteNames.pathProvider);
                },
                child: const Text("Path Provider")),
            const SizedBox(height: 20),
          ],
        )),
      ),
    );
  }
}
