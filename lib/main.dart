import 'package:concept_practice/routing/routes.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routerConfig: AppGoRoutes().router,
      // routerDelegate: AppGoRoutes().router.routerDelegate,
      // routeInformationParser: AppGoRoutes().router.routeInformationParser,
      // routeInformationProvider: AppGoRoutes().router.routeInformationProvider,
    );
  }
}
